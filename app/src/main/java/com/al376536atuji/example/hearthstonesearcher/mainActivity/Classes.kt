package com.al376536atuji.example.hearthstonesearcher.mainActivity

class Classes {

    private var id : Int
    private var name : String

    constructor(id: Int, name: String) {
        this.id=id
        this.name=name
    }

    fun getId() : Int {
        return id
    }
    fun setId (id : Int ) {
        this.id = id
    }

    fun getName(): String {
        return name
    }

    fun setName(name : String) {
        this.name  = name
    }




}