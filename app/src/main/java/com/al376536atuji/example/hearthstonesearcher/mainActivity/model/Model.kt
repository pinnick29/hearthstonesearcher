package com.al376536atuji.example.hearthstonesearcher.mainActivity.model

import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import com.android.volley.Response


class Model (context: Context) {

    private val network = Network.getInstance(context)

    fun getCards (listener : Response.Listener<ArrayList<Card>>,errorListener : Response.ErrorListener,
                  manaCost:Int,classHS:Int,typeHS:Int ){

        network.getCards(Response.Listener { netCards ->


            listener.onResponse(netCards)

        }, errorListener,manaCost,classHS,typeHS)

    }

    fun takeImageFromURL( card_id : String, listener: Response.Listener<Bitmap>,errorListener: Response.ErrorListener){
        return network.takeImageFromURL (card_id,Response.Listener { image -> listener.onResponse(image) },errorListener)
    }





}