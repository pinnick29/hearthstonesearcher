package com.al376536atuji.example.hearthstonesearcher.mainActivity

import android.view.View
import com.al376536atuji.example.hearthstonesearcher.mainActivity.model.Card

interface MainView {
    fun showError(message: String)
    fun getNames() : Array<String>
    fun getTypeNames() : Array<String>
    fun getManaSelected () : Int
    fun getTypeSelected () : Int
    fun getClassSelected () : Int
    fun doQuery (view: View)
    fun startCardsActivity (cards : ArrayList<Card>)
}