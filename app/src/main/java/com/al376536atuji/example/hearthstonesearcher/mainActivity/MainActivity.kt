 package com.al376536atuji.example.hearthstonesearcher.mainActivity

import android.content.Intent
import android.graphics.Color
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.NumberPicker
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import com.al376536atuji.example.hearthstonesearcher.CardsActivity.CardsActivity
import com.al376536atuji.example.hearthstonesearcher.R
import com.al376536atuji.example.hearthstonesearcher.mainActivity.model.Card
import com.al376536atuji.example.hearthstonesearcher.mainActivity.model.Model

class MainActivity : AppCompatActivity(),MainView {
    lateinit var textView : TextView
    lateinit var  numberPicker : NumberPicker
    lateinit var typePicker : NumberPicker
    lateinit var  classPicker : NumberPicker
    lateinit var presenter : Presenter
    var  classes = ArrayList<Classes>(10)
    var  types   = ArrayList <Classes> (10)

    @RequiresApi(Build.VERSION_CODES.Q)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setIcon(R.mipmap.ic_launcher)



        typePicker = findViewById(R.id.typePicker)
        classPicker = findViewById(R.id.classPicker)
        numberPicker = findViewById(R.id.numMana)

        val neutral =Classes(0, "Neutral")
        val druid =Classes(1, "Druída")
        val hunter =Classes(2, "Cazador")
        val mage =Classes(3, "Mago")
        val paladin =Classes(4, "Paladín")
        val priest =Classes(5, "Sacerdote")
        val rogue =Classes(6, "Pícaro")
        val shaman =Classes(7, "Chamán")
        val warrior =Classes(8, "Guerrero")
        val warlock =Classes(9, "Brujo")


        val spell = Classes (0,"Hechizo")
        val minion = Classes (1 ,"Esbirro")
        val weapon = Classes (2, "Arma")


        classes.add(neutral)
        classes.add(druid)
        classes.add(hunter)
        classes.add(mage)
        classes.add(paladin)
        classes.add(priest)
        classes.add(rogue)
        classes.add(shaman)
        classes.add(warrior)
        classes.add(warlock)

        //-------------------------------

        types.add(spell)
        types.add(minion)
        types.add(weapon)




        classPicker.maxValue = classes.size -1
        classPicker.minValue = 0
        classPicker.displayedValues = getNames()
        classPicker.textColor = Color.WHITE


        typePicker.maxValue = types.size -1
        typePicker.minValue = 0
        typePicker.displayedValues = getTypeNames()
        typePicker.textColor = Color.WHITE


        numberPicker.maxValue= 10
        numberPicker.minValue = 0
        numberPicker.value = 1
        numberPicker.textColor = Color.WHITE


        val model =
            Model(
                    applicationContext
            )

        presenter = Presenter (this , model)


        
    }

    override fun doQuery(view : View){
        presenter.cardsQuery()

    }

    override fun startCardsActivity(cards: ArrayList<Card>) {


        val intent = Intent(this, CardsActivity::class.java).apply {
            putExtra("cards", cards)
        }
        startActivity(intent)

    }

    override fun getTypeNames () : Array<String> {
        var names = Array<String >(10 ,{i:Int ->"$i"})
            for (i: Int in 0 until (types.size))
            names[i] = types[i].getName()

        return names
    }

    override fun getManaSelected(): Int {
        return numberPicker.value
    }

    override fun getTypeSelected(): Int {
       return typePicker.value
    }

    override fun getClassSelected(): Int {
        return classPicker.value
    }

    override fun showError(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }


    override fun getNames () : Array<String> {
        var names = Array<String >(10 ,{i:Int ->"$i"})
        for (i: Int in 0 until (classes.size))
            names[i] = classes[i].getName()

        return names
    }
}