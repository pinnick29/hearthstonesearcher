package com.al376536atuji.example.hearthstonesearcher.mainActivity

import android.content.Intent
import android.graphics.ColorSpace
import androidx.core.content.ContextCompat.startActivity
import com.al376536atuji.example.hearthstonesearcher.CardsActivity.CardsActivity
import com.al376536atuji.example.hearthstonesearcher.mainActivity.model.Card
import com.al376536atuji.example.hearthstonesearcher.mainActivity.model.Model
import com.android.volley.Response

class Presenter(val view: MainView, val model: Model) {

    private var manaCost: Int = 0
    private var classHS : Int =  0
    private var type : Int = 0

    fun getSelected(){
        manaCost = view.getManaSelected()
        classHS = view.getClassSelected()
        type = view.getTypeSelected()
    }


    fun cardsQuery (){
        getSelected()
        model.getCards(Response.Listener { cards :ArrayList<Card> ->
            if(cards != null && cards.size != 0){
                //Intent a la nueva actividad
                view.startCardsActivity(cards)
            }
            else{
                view.showError("No hay cartas con esas características.")

            }

        },Response.ErrorListener {error -> view.showError(error.toString())
        },manaCost,classHS,type)



    }



}