package com.al376536atuji.example.hearthstonesearcher.CardsActivity

import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.opengl.Visibility
import android.view.View
import android.view.View.VISIBLE
import android.widget.ImageView
import android.widget.ProgressBar
import com.al376536atuji.example.hearthstonesearcher.mainActivity.model.Model
import com.android.volley.Response


class CardsPresenter(val view: CardsActivity, val model: Model) {

    fun showImage( card_id : String,image : ImageView,progressBar : ProgressBar) {
        progressBar.visibility = View.VISIBLE
        model.takeImageFromURL(card_id, Response.Listener { card ->
            if (card != null){
                progressBar.visibility = View.GONE
                view.printImage(card, image)
            }
            else {
                view.showError("No existe imagen de esta carta.")
            }

        },
                Response.ErrorListener { error ->
                    view.showError(error.toString())

                })
    }


}