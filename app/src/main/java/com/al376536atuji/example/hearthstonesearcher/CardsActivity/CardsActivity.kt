package com.al376536atuji.example.hearthstonesearcher.CardsActivity

import android.annotation.SuppressLint
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.al376536atuji.example.hearthstonesearcher.R
import com.al376536atuji.example.hearthstonesearcher.mainActivity.model.Card
import com.al376536atuji.example.hearthstonesearcher.mainActivity.model.Model


private const val IMAGE_URL = "https://art.hearthstonejson.com/v1/render/latest/esES/512x/"

class CardsActivity : AppCompatActivity() {
    lateinit var cards : ArrayList<Card>
    lateinit var presenter: CardsPresenter
    lateinit var recyclerView : RecyclerView



    @SuppressLint("ResourceType")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cards)
        recyclerView = findViewById(R.id.recycler_view)

        cards = intent.getParcelableArrayListExtra<Card>("cards")!!


        for(i in 0..cards.size-1) {
            val card  = cards[i] as Card
            Log.d ("CARDS", "LA CARTA [$i] DE LA 2ACTIVIDAD SON: $card")
        }

        val model = Model(applicationContext)
        presenter = CardsPresenter(this, model)

        recyclerView.adapter = Adapter(cards) {card ->
            val builder: AlertDialog.Builder? = this.let {
                AlertDialog.Builder(it)
            }

            val customLayout : View = getLayoutInflater().inflate(R.layout.image, null);
            val image = customLayout.findViewById<ImageView>(R.id.imageView4)
            val progressBar = customLayout.findViewById<ProgressBar>(R.id.progressBar2)
            presenter.showImage(card.id,image,progressBar)




            builder?.setView(customLayout)




            //image.setImageBitmap(BitmapFactory.decodeStream(url.openConnection().getInputStream()))


            //builder?.setView(R.layout.image)

            //builder?.setIcon( )
            builder?.setMessage("Artista : ${card.artist}")?.setPositiveButton(
                    "OK"
            ) { d, _ ->
                d?.dismiss()
            }

            val dialog: AlertDialog? = builder?.create()




            dialog?.show()




        }
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.setHasFixedSize(true)

    }

    fun printImage ( card : Bitmap , image : ImageView){

        image.setImageDrawable(BitmapDrawable(Resources.getSystem(), card))
        image.scaleX = 1.2f
        image.scaleY = 1.2f


    }
    fun showError(str : String){
        Toast.makeText(this, str, Toast.LENGTH_LONG).show()
    }


}