package com.al376536atuji.example.hearthstonesearcher.mainActivity.model

import android.R.attr.bitmap
import android.content.Context
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.media.Image
import android.util.Log
import android.widget.ImageView
import com.al376536atuji.example.hearthstonesearcher.mainActivity.SingletonHolder
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.ImageRequest
import com.android.volley.toolbox.JsonArrayRequest
import com.android.volley.toolbox.Volley
import com.squareup.picasso.Picasso
import com.squareup.picasso.R
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.InputStream
import java.net.HttpURLConnection
import java.net.URL


private const val BASE_URL = "https://api.hearthstonejson.com/v1/25770/esES/cards.collectible.json"
private const val IMAGE_URL = "https://art.hearthstonejson.com/v1/render/latest/esES/512x/"
private const val CARD_CLASS = "cardClass"

private const val MANA_COST = "cost"
private const val TYPE_HS = "type"
private const val NAME = "name"
private const val ID = "id"
private const val ATTACK = "attack"
private const val HEALTH = "health"
private const val RARITY = "rarity"
private const val ARMOR = "armor"
private const val DURABILITY = "durability"

private const val ARTIST = "artist"



class Network private constructor(context: Context) {

    companion object: SingletonHolder<Network, Context>(:: Network)

    //var cacheDir: File = context.cacheDir
    //var cache: Cache=DiskBasedCache(cacheDir , 4 * 1024 * 1024) // 1MB cap
    //var network= BasicNetwork(HurlStack())

    val queue = Volley.newRequestQueue(context)
    var cardObject: JSONObject? = null
    //val queue= RequestQueue(cache , network)

    fun getCards (listener : Response.Listener<ArrayList<Card>>, errorListener : Response.ErrorListener,manaCost: Int,classHS: Int,typeHS: Int){
        val url = "$BASE_URL"


        Log.d ("","AQUIII" )
        val jsonObjectRequest = JsonArrayRequest(Request.Method.GET,url,null,
            {response -> processCards(response,listener,manaCost,classHS, typeHS) },
            {error -> errorListener.onErrorResponse(error) }
        )


        jsonObjectRequest.retryPolicy= DefaultRetryPolicy(600000, 1 , DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)


        queue.add(jsonObjectRequest)
    }

    fun longInfo(str: String) {
        if (str.length > 4000) {
            Log.d("CARDS", str.substring(0, 4000))
            longInfo(str.substring(4000))
        } else Log.d("CARDS", str)
    }

    fun takeImageFromURL(card_id : String ,listener: Response.Listener<Bitmap>,errorListener: Response.ErrorListener) {

        if (card_id.endsWith("e")) card_id.dropLast(1)
        val url = "${IMAGE_URL}"+ "${card_id}" + ".png"
        Log.d("CARDS", "El URL de card $card_id es: $url")
        val imageRequest = ImageRequest(url,{response-> listener.onResponse(response)} ,2000,2000,Bitmap.Config.ARGB_8888,
                {error ->errorListener.onErrorResponse(error)})

        queue.add(imageRequest)




    }




    private fun processCards(response: JSONArray, listener: Response.Listener<ArrayList<Card>>, manaCost: Int, classHS: Int, typeHS: Int ){
        //longInfo("$response" )

        val cards = ArrayList<Card>()

        //val cardsArray = response.getJSONArray(VERSION).getJSONObject(1).getJSONArray(CARDS)
        lateinit var hero : String
        lateinit var typeCard : String

        when (classHS){
            0 -> hero = "NEUTRAL"
            1 -> hero = "DRUID"
            2 -> hero = "HUNTER"
            3 -> hero = "MAGE"
            4 -> hero = "PALADIN"
            5 -> hero = "PRIEST"
            6 -> hero = "ROGUE"
            7 -> hero = "SHAMAN"
            8 -> hero = "WARRIOR"
            9 -> hero = "WARLOCK"
        }

        when (typeHS){

            0 -> typeCard = "SPELL"
            1 -> typeCard = "MINION"
            2 -> typeCard = "WEAPON"

        }

        try {
            var j : Int =0
            for(i in 0..response.length()-1) {
                cardObject = response[i] as JSONObject
                //Log.d("CARDS", "El card object de response[$i] es: $cardObject")

                if (hero == cardObject!!.optString(CARD_CLASS) &&
                    manaCost == cardObject!!.optInt(MANA_COST) &&
                    typeCard == cardObject!!.optString(TYPE_HS)
                ) {

                    val id = cardObject!!.optString(ID)
                    val name: String = cardObject!!.optString(NAME)
                    val rarity: String? = cardObject?.optString(RARITY)
                    val attack: Int? = cardObject?.optInt(ATTACK,0)

                    val health: Int? = cardObject?.optInt(HEALTH,0)
                    val armor: Int? = cardObject?.optInt(ARMOR,0)
                    val durability: Int? = cardObject?.optInt(DURABILITY,0)
                    val artist = cardObject!!.optString(ARTIST)

                    val aux = Card(
                        id,
                        typeCard,
                        manaCost,
                        name,
                        rarity,
                        attack,
                        health,
                        armor,
                        durability,
                        artist
                    )

                    cards.add(aux)
                    Log.d("CARDS", "EL VLAOR DE AUX ${j++} ES : $aux")
                }

            }
                //Log.d("CARDS","CARTAS: $cards")
                //cards.sortBy {card -> card.name  }

                Log.d ("CARDS", "CARTAS : $cards ")

                listener.onResponse(cards)


        }
        catch (e : JSONException){
            listener.onResponse(null)

        }


    }


}