package com.al376536atuji.example.hearthstonesearcher.mainActivity.model

import android.os.Parcel
import android.os.Parcelable

data class Card(val id: String, val type: String, val manaCost: Int, val name:String, val rarity:String?,
                val attack: Int?, val health: Int?, val armor: Int?, val durability: Int?,val artist : String ) : Parcelable,Comparable<Card>  {
    constructor(parcel: Parcel) : this(
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readInt()!!,
        parcel.readString()!!,
        parcel.readString(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readString()!!

    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(type)
        parcel.writeInt(manaCost)
        parcel.writeString(name)
        parcel.writeString(rarity)
        attack?.let { parcel.writeInt(it) }
        health?.let { parcel.writeInt(it) }
        armor?.let  { parcel.writeInt(it) }
        durability?.let { parcel.writeInt(it) }
        parcel.writeString(artist)


    }
    override fun compareTo(other: Card): Int {
        return  when{
            name ==other.name ->0
            name < other.name -> -1
            else -> 1
        }
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Card> {
        override fun createFromParcel(parcel: Parcel): Card {
            return Card(parcel)
        }

        override fun newArray(size: Int): Array<Card?> {
            return arrayOfNulls(size)
        }
    }
}


