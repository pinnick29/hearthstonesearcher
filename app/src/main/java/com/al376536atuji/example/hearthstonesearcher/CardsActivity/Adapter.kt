package com.al376536atuji.example.hearthstonesearcher.CardsActivity

import android.media.Image
import android.provider.ContactsContract
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.al376536atuji.example.hearthstonesearcher.R
import com.al376536atuji.example.hearthstonesearcher.mainActivity.model.Card



class Adapter(private val exampleList: ArrayList <Card>, val onClickListener : (Card) -> Unit ) : RecyclerView.Adapter<Adapter.ViewHolder>(){

    class ViewHolder (itemView : View) : RecyclerView.ViewHolder (itemView){

        val manaCost: TextView = itemView.findViewById(R.id.manaNumber)
        val name: TextView = itemView.findViewById(R.id.nameCard)
        val rarityCommon : ImageView = itemView.findViewById(R.id.commonShardIMG)
        val rarityUncommon : ImageView = itemView.findViewById(R.id.uncommonShardIMG)
        val rarityEpic : ImageView = itemView.findViewById(R.id.epicShardIMG)
        val rarityLegendary : ImageView = itemView.findViewById(R.id.legendaryShardIMG)
        val attack: TextView = itemView.findViewById(R.id.attackNumber)
        val attackIMG : ImageView = itemView.findViewById(R.id.attackIMG)
        val durability: TextView = itemView.findViewById(R.id.durabilityNumber)
        val durabilityIMG : ImageView = itemView.findViewById(R.id.weaponDurabilityIMG)
        val health : TextView = itemView.findViewById(R.id.healthNumber)
        val healthIMG : ImageView = itemView.findViewById(R.id.healthIMG)
        val weaponAttack: TextView = itemView.findViewById(R.id.weaponAttackNumber)
        val weaponAttackIMG : ImageView = itemView.findViewById(R.id.weaponAttackIMG)



        fun bind (item  : Card){

            Log.d("LIST", "El item es :  $item ")
            when (item.rarity){
                "COMMON" -> {
                            rarityCommon.visibility = VISIBLE
                            rarityUncommon.visibility = GONE
                            rarityEpic.visibility = GONE
                            rarityLegendary.visibility = GONE


                            }
                "UNCOMMON"->{

                            rarityCommon.visibility = GONE
                            rarityUncommon.visibility = VISIBLE
                            rarityEpic.visibility = GONE
                            rarityLegendary.visibility = GONE

                            }
                "EPIC" -> {
                            rarityCommon.visibility = GONE
                            rarityUncommon.visibility = VISIBLE
                            rarityEpic.visibility = GONE
                            rarityLegendary.visibility = GONE

                }
                "LEGENDARY" -> {
                            rarityCommon.visibility = GONE
                            rarityUncommon.visibility = GONE
                            rarityEpic.visibility = GONE
                            rarityLegendary.visibility = VISIBLE

                }


            }

            Log.d("LIST","RAREZA OK")

            Log.d("LIST","MANA : ${item.manaCost}")
            Log.d("LIST", "NOMBRE : ${item.name}")

            manaCost.setText(item.manaCost.toString())
            name.setText(item.name)

            when (item.type){
                "SPELL" -> {
                            attack.visibility =GONE
                            attackIMG.visibility = GONE
                            durability.visibility =GONE
                            durabilityIMG.visibility= GONE
                            health.visibility = GONE
                            healthIMG.visibility = GONE
                            weaponAttack.visibility = GONE
                            weaponAttackIMG.visibility = GONE
                            }
                "MINION" -> {
                    attack.visibility = VISIBLE
                    attackIMG.visibility = VISIBLE
                    durability.visibility =GONE
                    durabilityIMG.visibility= GONE
                    health.visibility =  VISIBLE
                    healthIMG.visibility = VISIBLE
                    weaponAttack.visibility = GONE
                    weaponAttackIMG.visibility = GONE
                    attack.setText(item.attack!!.toString())
                    health.setText(item.health!!.toString())

                }
                "WEAPON" -> {
                    attack.visibility = GONE
                    attackIMG.visibility = GONE
                    durability.visibility = VISIBLE
                    durabilityIMG.visibility= VISIBLE
                    health.visibility = GONE
                    healthIMG.visibility = GONE
                    weaponAttack.visibility = VISIBLE
                    weaponAttackIMG.visibility = VISIBLE
                    weaponAttack.setText(item.attack!!.toString())
                    durability.setText(item.durability!!.toString())

                }
            }


        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(
                R.layout.card,
                parent, false  )

        return ViewHolder(
                itemView
        )
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val currentItem = exampleList[position]

        holder.itemView.setOnClickListener{
            onClickListener(exampleList[position])
        }

        holder.bind(currentItem)


    }

    override fun getItemCount() = exampleList.size


}